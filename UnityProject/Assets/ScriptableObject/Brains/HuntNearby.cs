﻿using UnityEngine;
using System.Collections;
using System.Linq;

[CreateAssetMenu(menuName = "Brains/Hunt Nearby")]
public class HuntNearby : TankBrain
{
    [MinMaxRange(0, 10)]
    public float viewRange;

    public float lookSphereCastRadius = 5;
    private RaycastHit hit;

    public float aimAngleThreshold = 2f;

    [MinMaxRange(0, 0.05f)]
    public RangedFloat chargeTimePerDistance;

    [MinMaxRange(0, 10)]
    public RangedFloat timeBetweenShots;

    public override void Think(TankThinker tank)
    {
        GameObject target = tank.Remember<GameObject>("target");
        var movement = tank.GetComponent<TankMovement>();

        // Draw the view range of the tank
        Debug.DrawRay(tank.transform.position, tank.transform.forward.normalized * viewRange, Color.red);
        if (!target)
        {
            // Find the nearest tank that isn't me only if is nearby within my viewRange
            if (Physics.SphereCast(tank.transform.position, lookSphereCastRadius, tank.transform.forward, out hit, viewRange)
            && hit.collider.CompareTag("Player"))
            {
                target = hit.collider.gameObject;
                tank.Remember<GameObject>("target");
            }
        }

        if (!target)
        {
            // No targets left - drive in a victory circles
            movement.Steer(0.5f, 1f);
            return;
        }

        // aim at the target
        Vector3 desiredForward = (target.transform.position - tank.transform.position).normalized;
        if (Vector3.Angle(desiredForward, tank.transform.forward) > aimAngleThreshold)
        {
            bool clockwise = Vector3.Cross(desiredForward, tank.transform.forward).y > 0;
            movement.Steer(0f, clockwise ? -1 : 1);
        }
        else
        {
            // Stop
            movement.Steer(0f, 0f);
        }

        // Fire at the target
        var shooting = tank.GetComponent<TankShooting>();
        if (!shooting.IsCharging)
        {
            if (Time.time > tank.Remember<float>("nextShotAllowedAfter"))
            {
                float distanceToTarget = Vector3.Distance(target.transform.position, tank.transform.position);
                float timeToCharge = distanceToTarget * Random.Range(chargeTimePerDistance.minValue, chargeTimePerDistance.maxValue);
                tank.Remember("fireAt", Time.time + timeToCharge);
                shooting.BeginChargingShot();
            }
        }
        else
        {
            float fireAt = tank.Remember<float>("fireAt");
            if (Time.time > fireAt)
            {
                shooting.FireChargedShot();
                tank.Remember("nextShotAllowedAfter", Time.time + Random.Range(timeBetweenShots.minValue, timeBetweenShots.maxValue));
            }
        }
    }
}