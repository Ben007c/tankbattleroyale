﻿using UnityEngine;

public abstract class TankBrain : ScriptableObject
{
    public bool IsNPC;

    public virtual void Initialize(TankThinker tank)
    {
    }

    public abstract void Think(TankThinker tank);
}