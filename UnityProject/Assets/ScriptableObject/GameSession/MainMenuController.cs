﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;

public class MainMenuController : MonoBehaviour
{
    public GameSettings GameSettingsTemplate;
    public GameObject m_PlayerInfoEntryPrefab;
    public Color[] AvailableColors;

    public TankBrain[] AvailableTankBrains;

    public UnityEngine.UI.Button PanelSwitcher;
    public GameObject PlayersPanelScrollView;   // Root Object That holds the whole Scroll View of Player info entries
    public GameObject PlayersPanel;             // Panel That Contains the Player Info Entries
    public GameObject SettingsPanel;

    public UnityEngine.UI.Text NumberOfRoundsLabel;
    public UnityEngine.UI.Slider NumberOfRoundsSlider;
    public UnityEngine.UI.Text NumberOfPlayersLabel;
    public UnityEngine.UI.Slider NumberOfPlayersSlider;

    public string SavedSettingsPath
    {
        get
        {
            return System.IO.Path.Combine(Application.persistentDataPath, "tanks-settings.json");
        }
    }

    private void Start()
    {
        if (System.IO.File.Exists(SavedSettingsPath))
            GameSettings.LoadFromJSON(SavedSettingsPath);
        else
            GameSettings.InitializeFromDefault(GameSettingsTemplate);

        OnChangeNumberOfPlayers(GameSettings.Instance.NumberOfPlayers);
        OnChangeNumberOfRounds(GameSettings.Instance.NumberOfRounds);

        // foreach (var info in GetComponentsInChildren<PlayerInfoController>())
        //     info.Refresh();

        // NumberOfRoundsSlider.value = GameSettings.Instance.NumberOfRounds;
        //NumberOfPlayersSlider.value = GameSettings.Instance.NumberOfPlayers;
    }

    public void Play()
    {
        GameSettings.Instance.SaveToJSON(SavedSettingsPath);
        GameState.CreateFromSettings(GameSettings.Instance);
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public Color GetNextColor(Color color)
    {
        if (color == Color.clear) { return AvailableColors[0]; }
        int existingColor = Array.FindIndex(AvailableColors, c => c == color);
        if (existingColor == -1) { }
        existingColor = (existingColor + 1) % AvailableColors.Length;
        return AvailableColors[existingColor];
    }

    public TankBrain GetNextBrain(TankBrain brain = null)
    {
        if (brain == null)
            return AvailableTankBrains[0];

        int index = Array.FindIndex(AvailableTankBrains, b => b == brain);

        index++;
        return (index < AvailableTankBrains.Length) ? AvailableTankBrains[index] : null;
    }

    public void EnforceSameColorForAllNPCs(Color color)
    {
        foreach (var info in GameSettings.Instance.players)
        {
            if (info.Brain != null && info.Brain.IsNPC)
                info.Color = color;
        }
        foreach (var info in GetComponentsInChildren<PlayerInfoController>())
            info.Refresh();
    }

    public void OnChangeNumberOfRounds(float value)
    {
        GameSettings.Instance.NumberOfRounds = (int)value;

        NumberOfRoundsLabel.text = GameSettings.Instance.NumberOfRounds.ToString();
        NumberOfRoundsSlider.value = GameSettings.Instance.NumberOfRounds;
    }

    public void OnChangeNumberOfPlayers(float value)
    {
        if (value < 0)
        {
            return;
        }
        //GameSettings.Instance.NumberOfPlayers = (int)value;
        int diff = PlayersPanel.transform.childCount - (int)value;

        while (diff != 0)
        {
            if (diff > 0)
            {
                if (GameSettings.Instance.players.Count > 0 && PlayersPanel.transform.childCount > 0)
                {
                    GameSettings.Instance.players.RemoveAt(GameSettings.Instance.players.Count - 1);
                    DestroyImmediate(PlayersPanel.transform.GetChild(PlayersPanel.transform.childCount - 1).gameObject);
                }
                diff--;
            }
            else if (diff < 0)
            {
                PlayerInfoController p = Instantiate(m_PlayerInfoEntryPrefab, PlayersPanel.transform)
                     .GetComponent<PlayerInfoController>();

                p.PlayerIndex = PlayersPanel.transform.childCount - 1;
                if ((int)value > GameSettings.Instance.NumberOfPlayers)
                    GameSettings.Instance.players.Add(new GameSettings.PlayerInfo("", Color.clear, ""));

                p.gameObject.SetActive(true);
                diff++;
            }
        }

        NumberOfPlayersLabel.text = GameSettings.Instance.NumberOfPlayers.ToString();
        NumberOfPlayersSlider.value = GameSettings.Instance.NumberOfPlayers;
    }

    public void OnSwitchPanels()
    {
        PlayersPanelScrollView.SetActive(!PlayersPanelScrollView.activeSelf);
        SettingsPanel.SetActive(!SettingsPanel.activeSelf);
        PanelSwitcher.GetComponentInChildren<UnityEngine.UI.Text>().text = PlayersPanel.activeSelf ? "Settings" : "Players";
    }
}