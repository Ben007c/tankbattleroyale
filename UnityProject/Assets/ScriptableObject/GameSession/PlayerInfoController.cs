﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerInfoController : MonoBehaviour
{
    public int PlayerIndex;

    public UnityEngine.UI.InputField PlayerName;
    public UnityEngine.UI.Button PlayerColor;
    public UnityEngine.UI.Button PlayerBrain;

    private MainMenuController _mainMenu;
    private GameSettings.PlayerInfo _player;

    public void OnEnable()
    {
        _mainMenu = GetComponentInParent<MainMenuController>();
        Refresh();
    }

    public void Refresh()
    {
        _player = GameSettings.Instance.players[PlayerIndex];

        PlayerName.text = _player.Name;

        var colorBlock = PlayerColor.colors;
        colorBlock.normalColor = _player.Color;
        colorBlock.highlightedColor = _player.Color;
        PlayerColor.colors = colorBlock;

        PlayerBrain.GetComponentInChildren<UnityEngine.UI.Text>().text = (_player.Brain != null)
            ? _player.Brain.name
            : "None";
    }

    public void OnNameChanged()
    {
        _player.Name = PlayerName.text;
    }

    public void OnCycleColor()
    {
        Color nextColor = _mainMenu.GetNextColor(_player.Color);
        _player.Color = nextColor;
        if (_player.Brain != null && _player.Brain.IsNPC)
        {
            _mainMenu.EnforceSameColorForAllNPCs(_player.Color);
        }

        Refresh();
    }

    public void OnCycleBrain()
    {
        //can return a null brain that will be displayed as "None"
        TankBrain nextBrain = _mainMenu.GetNextBrain(_player.Brain);
        _player.Brain = nextBrain;
        if (nextBrain != null && nextBrain.IsNPC)
        {
            _mainMenu.EnforceSameColorForAllNPCs(_player.Color);
        }

        Refresh();
    }
}