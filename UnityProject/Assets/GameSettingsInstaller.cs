using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "Installers/GameSettingsInstaller")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
{
    public TankThinker TankPrefab;             // Reference to the prefab the players will control.
    public Rigidbody Shell;                   // Prefab of the shell.

    public override void InstallBindings()
    {
        Container.BindFactory<TankThinker, TankThinker.Factory>()
            .FromComponentInNewPrefab(TankPrefab)
            .WithGameObjectName("Tank")
            .UnderTransformGroup("Tanks");

        Container.BindFactory<ShellExplosion, ShellExplosion.Factory>()
            .FromPoolableMemoryPool<ShellExplosion, ShellExplosionPool>(poolBinder => poolBinder
             .WithInitialSize(5)
             .WithMaxSize(5)
             .FromComponentInNewPrefab(Shell)
             .UnderTransformGroup("Shells"));
    }

    private class ShellExplosionPool : MonoPoolableMemoryPool<IMemoryPool, ShellExplosion>
    {
    }
}